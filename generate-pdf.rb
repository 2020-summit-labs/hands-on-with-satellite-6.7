#!//usr/bin/ruby
# To install and run
# $ bundle install
# $ bundle exec generate-pdf.rb


require 'yaml'

workshop = YAML.load(File.read("workshop/workshop.yaml"))
modules = YAML.load(File.read("workshop/modules.yaml"))["modules"]
sample_vars = {
    "USER_GUID" => "acdc",
    "DNSZONE"=> "",
    "SATELLITE_HOSTNAME" => "satellite.acdc.example.com",
    "SATELLITE_USERNAME" => "summit-admin",
    "SATELLITE_PASSWORD" => "summit2020"
}

ignorables = [/\:markup-in-source.*/, /\:show_solution.*/, /\:sectlinks.*/, /\,subs\=\"\{markup-in-source\}\"/]

title = "Lab Guide"

summary_adoc = workshop["modules"]["activate"].map do |item|
    fl = File.read("workshop/content/#{item}.adoc")
    fl.gsub!(/\=\= \w+/) do |word|
        "=#{word}"
    end
    fl.gsub!(/\#\s*\*(.+)\*/, "# \\1")

    ignorables.each {|pattern| fl.gsub!(pattern, "")}

    sample_vars.each do |name, value|
        fl.gsub!(/\:#{name}.*/, "")
        fl.gsub!("{#{name}}", value)
    end

    "== #{modules[item]["name"]} \n\n #{fl}"
end.join("\n")

summary_adoc = %{
[align=center]
= *#{title}*
:doctype: book
:sectnums:
:toc:
:toclevels: 3
:title-logo-image: image:assets/prepare-environment/images/summit-2020.png[align=center]

#{summary_adoc}
}
tmp_file = 'workshop/content/.summary.adoc'
File.write(tmp_file, summary_adoc)
out_file = 'satellite-lab-guide.pdf'
system("asciidoctor-pdf #{tmp_file} -o #{out_file}")

puts("Generated #{out_file}")
File.delete tmp_file


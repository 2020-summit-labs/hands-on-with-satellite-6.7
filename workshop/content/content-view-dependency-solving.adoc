:sectlinks:

*Note:* This section is expected to take over 10 minutes to complete and is not a common scenario for Satellite administrators. Feel free to move on if you're not interested in Content View Dependency Solving via Content Views.

== Dependency Solving in Content Views


_Satellite 6.6_ introduced functionality to support RPM Dependency Solving during Content View publishes. This feature is useful for Content Views that have Package or Erratum Filters defined. The publish operation on a Content View with `Dependency Solving` enabled will not only copy the list of Packages defined by filters, but also copy all dependent Packages, Errata and Module Streams along with them.

__Satellite 6.7__ enhances the existing Content View Dependency solving with a couple of important changes.

* Enables copying dependent Module Streams in addition to Packages and Errata.
* During dependency solving, __All__ the repositories belonging to a Content View are considered in combination when determining the Packages, Errata or Module Streams to be copied. This change is primarily due to the relationship between `Base OS` and `App Stream` repositories in Red Hat Enterprise Linux 8. The Module Streams in the `App Stream` repository typically have dependencies on `Base OS` repositories that also need to get copied.

== Exercise

*Problem* As a Satellite administrator you are asked to create a Content View with the following criteria.

* Includes the following repositories
** `Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8`
** `Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8` repository
* Includes the the following Module and its dependencies
** name: `ant`
** stream: `1.10`
** version: `820181213135032`
** release: `5ea3b708`

In this exercise we will

* Create the `RHEL8-DepSolver` Content View
* Add the appropriate Repositories to the Content View.
* Add a Module Stream filter to include `ant` Module Stream.
* Add a Package filter to exclude non-modular RPMs. (This step is needed because we want to limit this Content View to only RPMs belonging to the `ant` Module Stream and their dependent RPMs.)
* Publish the Content View without `Dependency Solving` turned on.
* Explore the outcome.
* Publish the Content View with `Dependency Solving` turned on.
* Explore the outcome.

=== Create new Content View

* Select `Content` > `Content View`
* Select `Create New View`
* Create a Content View with the name `RHEL8-DepSolver`

image:assets/content-view-depsolve/images/01-CreateNewCV.png[]

* Select `Save`
* This should take you to the `Add Yum Repositories` page for `RHEL8-DepSolver` Content View.
* Select the following Repositories
** `Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8`
** `Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8`

image:assets/content-view-depsolve/images/02-AddRepos.png[]

* Select `Add Repositories` on the top right. This should add the selected repositories to the Content View.
* Select `List/Remove` tab.
* Make sure the following Repositories have been added.
** `Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8`
** `Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8`

* Select the `Details` tab.
* Verify `Solve Dependencies` is set to `No`.

=== Add Module Stream Filter

* Select the arrow next to the `Yum Content` tab and select `Filters` from the dropdown.
* Select `New Filter`
* Create a Module Stream filter with the following details
 ** Name - `Include module stream ant`
 ** Content Type - `Module Stream`
 ** Inclusion Type - `Include`

image:assets/content-view-depsolve/images/07-AddModuleFilter.png[]

* Select `Save`. This should lead you to a page listing the Module Streams available to this Content View.
* Select the following Module Stream
** name: `ant`
** stream: `1.10`
** version: `820181213135032`
** release: `5ea3b708`

image:assets/content-view-depsolve/images/09-AddModuleFilter-Details.png[]

* Select `Add Module Stream`
* Select the `List/Remove` tab.
* Make sure you see the 'ant' module among the Module Streams list.

=== Add Package Filter

* Select the arrow next to the `Yum Content` tab and select `Filters` from the dropdown.
* Create a Package filter with the following details
** Name - `Exclude all rpms`
** Content Type - `Package`
** Inclusion Type - `Exclude`

image:assets/content-view-depsolve/images/12-AddExcludeAllFilter.png[]

* Select `Add Rule` in the following screen to add a new package filter rule.
* Under `RPM Name` add `*`
* Select `Save`.

image:assets/content-view-depsolve/images/13-AddExcludeAllFilter.png[]

* Select `Publish New Version` on top right
* Fill out the description with `Filters: Include module ant:1.10, Exclude all non-modular rpms`
* Select `Save`
* Observe the progress bar on the Versions page and wait for the Publish operation to complete.

image:assets/content-view-depsolve/images/14-Publishv1.png[]

_Notice_ that the new version has 2 Packages and 1 module stream. This result is borne out of the include Module Stream Filters and  exclude non-modular Packages filter.

* Select `Version 1.0` link from the Versions table.
* Select the `Yum Repositories` tab.

image:assets/content-view-depsolve/images/15-Publishv1-YumRepos.png[]

_Notice_ that the 2 Packages and 1 Module Stream in the AppStream Repository while the Base OS Repository is empty.


=== Enable Dependency Solving

* Select the `Details` tab.
* Hover the cursor over `No` across the `Solve Dependencies` label and click on it to edit.
* Select the checkbox next to `Solve Dependencies`
* Select `Save`
* Make sure `Solve Dependencies` is set to `Yes`

image:assets/content-view-depsolve/images/17-SolveDependencies.png[]

* Select `Publish New Version` on top right
* Give a meaningful description like `Solve Dependencies: Yes, Filters: Include module ant:1.10, Exclude all non-modular rpms`
* Select `Save`.

image:assets/content-view-depsolve/images/18-Publish.png[]

_Note:_ The publish operation should complete in around 5 minutes.

Continue along to the next section while the Content View Publish operation is running. We will revisit it later in the link:wrapup#_content_view_dependency_solving_follow_up[Follow Up] section to check the results.

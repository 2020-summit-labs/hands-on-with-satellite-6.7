:USER_GUID: %guid%
:DNSZONE: NOGO
:SATELLITE_HOSTNAME: %satellite_hostname%
:sectlinks:
:markup-in-source: verbatim,attributes,quotes

== Getting Familiar with Satellite

In this introductory section we will briefly familiarize you with the web user interface of Red Hat Satellite. The goal is to navigate to the major components and also view the pre-populated data for the lab. This section is optional for users with prior experience with Red Hat Satellite, feel free to continue to the next section.

When you log in, you are presented with a monitoring dashboard and a navigation system:

image:assets/satellite-overview/images/summit_satellite_dashboard.png[summit-dashboard]

The main bar at the top of the page contains information on the currently managed Organization and Location. The bar also includes a notification drawer informing on recent system events and the user control on the right.

Satellite has a robust Role Based Access Control (RBAC) system which controls what actions users can take, and the resources (e.g repositories) and organizations. which they can access. For the sake of simplicity in this lab, we will use the Admin User account which has access to all the resources and organizations by default.

The vertical menu navigation on the left provides access to core functions of Red Hat Satellite including system provisioning, configuration and content management. Let's look closer at selected components:

=== Content Related components

Navigate to `Content > Subscriptions` to view the list of subscriptions imported to the Satellite. These subscriptions enable synchronizing content from the Red Hat CDN. Select `Red Hat Enterprise Linux Server, Standard (Physical or Virtual Nodes)` to see the details of the subscription, including the provided products.

*You may see SSL errors on this page as some communications are blocked to prevent modifying the manifest. These errors will not be present in normal Satellite installs, please ignore them for the duration of the lab*

*You may also see disconnected mode error as the communication to Content Delivery Network has been blocked to prevent download the packages from their. These errors will not be present in connected Satellite mode, please ignore them for the duration of the lab.*

image:assets/satellite-overview/images/subscriptions.png[Subscriptions]

Navigate to `Content > Products` to observe the Products (collections of repositories) enabled by current subscriptions. Selecting `Red Hat Enterprise Linux for x86_64` reveals the associated repositories that you can explore further.

image:assets/satellite-overview/images/products-repos.png[Products and repositories]

Navigate to `Content > Content Views` to see the currently existing Content Views (filtered views of content). Content Views can be promoted across Lifecycle Environments to provide a controlled deployment model where different machines are updated based on different schedules. For customers who do not wish to use Content Views, the Library contains a default Content View with the latest version of all content. Select `RHEL 8 Content` to explore the Content View details.

image:assets/satellite-overview/images/content-views.png[Content Views]

=== Provisioning Related components

Navigate to `Configure > Host Groups` to list the existing Host Groups that act as recipes for systems, describing how the system should be provisioned. When deploying a Host into a location either on bare metal or in your hybrid cloud, the provisioning parameters are inherited from the Host Group. Select `rhel-8` to view the associated provisioning parameters for example the operating system (then select `Cancel` to leave the Host Group unchanged).

image:assets/satellite-overview/images/hostgroups.png[Host Groups]

The provisioning parameters can be viewed under `Hosts` (e.g. Operating Systems) and `Infrastructure` (e.g. Domains or Subnets) menu items.

Navigate to `Infrastructure > Compute Resources` to view the Compute Resources available for provisioning. Satellite supports the Hybrid Cloud model by allowing users to provision machines onto both bare metal machines and onto cloud fabrics such as EC2, OpenStack, RHV, VMware, etc. Compute Resources model the connection between Satellite and the fabric. Select `LibvirtLocal` to view the existing Libvirt Compute Resource. To check if the Compute Resource is ready select `Edit` and then `Test Connection` (afterwards select `Cancel` to leave the Compute Resource unchanged).

image:assets/satellite-overview/images/compute-resources.png[Compute Resource]

Navigate to `Hosts > All Hosts` to see the list of all currently managed hosts. Select `skylab` to explore the Host details.

_We will now start the lab stories, which will guide you through performing operations with Satellite._

=== satellite-maintain to check running services

In Satellite, you can check if all underlying services are running correctly. We will be using Red Hat Web Console on the Satellite itself to execute commands.

* Select `Hosts > All Hosts`
* Select the `{SATELLITE_HOSTNAME}` Host.
* Select `Web Console` from the toolbar on the Host detail page.
** This will open the Web Console in a separate window or tab.
* In Web Console, Select `Terminal` from the nav bar on the left.

image:assets/satellite-overview/images/27-CockpitUI.png[]

** This will take you to the root terminal for Satellite.
** _Note_ Running root commands on the Satellite terminal from the Web Console is dangerous. The default Satellite install does not automatically enable this. It is enabled here for demo purposes.

* Execute the following command to check Satellite's services:

.Run
[source,bash,subs="{markup-in-source}"]
----

[root@satellite ~]# *satellite-maintain service status -b*
----

.Output
[source,bash,subs="{markup-in-source}"]
----

Running Status Services
================================================================================
Get status of applicable services:
Displaying the following service(s):

rh-mongodb34-mongod, postgresql, qdrouterd, qpidd, squid, pulp_celerybeat, pulp_resource_manager, pulp_streamer, pulp_workers, smart_proxy_dynflow_core, tomcat, dynflowd, httpd, puppetserver, foreman-proxy
- displaying rh-mongodb34-mongod                   [OK]
\ displaying postgresql                            [OK]
\ displaying qdrouterd                             [OK]
\ displaying qpidd                                 [OK]
\ displaying squid                                 [OK]
\ displaying pulp_celerybeat                       [OK]
\ displaying pulp_resource_manager                 [OK]
\ displaying pulp_streamer                         [OK]
\ displaying pulp_workers                          [OK]
\ displaying smart_proxy_dynflow_core              [OK]
\ displaying tomcat                                [OK]
\ displaying dynflowd                              [OK]
\ displaying httpd                                 [OK]
\ displaying puppetserver                          [OK]
\ displaying foreman-proxy                         [OK]
\ All services are running                         [OK]
--------------------------------------------------------------------------------
----

* You can use `satellite-maintain` to do other operations like upgrading Satellite when a new release shows up,  restarting services, installing and managing Packages, and several other operations. Run `satellite-maintain --help` to get a list of available options.

[source,bash,subs="{markup-in-source}"]
----

[root@satellite ~]# satellite-maintain --help
Usage:
    satellite-maintain [OPTIONS] SUBCOMMAND [ARG] ...

Parameters:
    SUBCOMMAND                    subcommand
    [ARG] ...                     subcommand arguments

Subcommands:
    health                        Health related commands
    upgrade                       Upgrade related commands
    service                       Control applicable services
    backup                        Backup server
    restore                       Restore a backup
    packages                      Lock/Unlock package protection, install, update
    advanced                      Advanced tools for server maintenance
    maintenance-mode              Control maintenance-mode for application

Options:
    -h, --help                    print help

----

To head towards the next section, first close the Red Hat Web Console window or tab and head back to the Satellite web UI window.
